# Owners API

This is a simple api use to make CRUD operations on Owner data. Created using nodejs + mySql database...

### Installation

```sh
$ git clone https://gitlab.com/smit.vekariya/owners_api_using_node-mysql.git
$ cd owners_api_using_node-mysql
$ npm install
```

### Setup .env

###### create .env file to / of project directiory and paste below 4 line in that file with valid data

```sh
HOST=localhost
USER=root
PASSWORD=password
DATABASE=ownersapi
PHOTO_UPLOAD_PATH=./public/images
```

### create DATABASE and TABLE

```sh
CREATE DATABASE ownersAPI;
USE ownersapi;

CREATE TABLE hobbies_names (
	hobbies_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    primary key (hobbies_id)
);

CREATE TABLE owners(
				owner_id INT NOT NULL AUTO_INCREMENT UNIQUE,
				name VARCHAR(50) NOT NULL,
                email VARCHAR(50) NOT NULL UNIQUE,
                mobile_number VARCHAR(10) NOT NULL UNIQUE,
                bio VARCHAR(500),
                active_status BOOLEAN DEFAULT false,
                role ENUM('student', 'teacher', 'contractor') NOT NULL DEFAULT 'student',
                gender ENUM('male', 'female', 'other') NOT NULL,
                photo VARCHAR(100) NOT NULL DEFAULT 'user.png',
                created_at DATETIME DEFAULT now(),
                PRIMARY KEY (owner_id)
                );

CREATE TABLE hobbies (
	hobbies_id INT NOT NULL,
    owner_id INT NOT NULL,
    primary key (hobbies_id, owner_id),
    FOREIGN KEY (hobbies_id) REFERENCES hobbies_names(hobbies_id) on delete cascade on update cascade,
	FOREIGN KEY (owner_id) REFERENCES owners(owner_id) on delete cascade on update no action
);


-- Sample data

INSERT INTO hobbies_names values (default, 'reading'),
 (default, 'writting'),
 (default, 'designing'),
 (default, 'swimming'),
 (default, 'developing'),
 (default, 'skydiving');

insert into owners values(default, 'smit', 'smit@gmail.com', '1234567890', 'Hello, I am a full stack developer..', true, 'teacher', 'male',default, default)
, (default, 'jay', 'jay@gmail.com', '9876543210', 'Hello, I am noob..', true, 'student', 'male', default , default );


insert into hobbies values(1, 1), (2, 1), (3, 1);
insert into hobbies values(4,2), (5,2);
```

### Run server

```sh
$ npm start
```
