const mysql = require("mysql");

const dbCon = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
});

dbCon.connect((err) => {
  if (err) console.log("Database connection faild");
  console.log("Database connected ");
});

module.exports = dbCon;
