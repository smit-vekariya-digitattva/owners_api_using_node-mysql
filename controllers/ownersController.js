const fs = require("fs");
const path = require("path");

const Owner = require("../model/Owner");
const validator = require("validator");

const handelFile = (req, res) => {
  const photo = req.files.photo;

  if (!photo.mimetype.startsWith("image/")) {
    return res.status(400).json({
      success: false,
      message: "Please upload an image file...",
    });
  }

  if (photo.size > 1000000) {
    return res.status(400).json({
      success: false,

      message: "Please upload an image less than 1mb...",
    });
  }
  photo.name = `${Date.now()}_${photo.name}`;
  req.body.photo = photo.name;
};

const movePhotoToStorage = (photo) => {
  photo.mv(`${process.env.PHOTO_UPLOAD_PATH}/${photo.name}`, (err) => {
    if (err) {
      return res.status(500).json({
        success: false,

        message: "problem with file upload",
      });
    }
  });
};

const deletePhotoFromStorage = (photo) => {
  if (photo !== "user.png") {
    fs.unlink(
      `${path.join(__dirname, "..", "public", "images", photo)}`,
      (err) => {
        if (err) console.log("something went wrong ( No problem )");
        else console.log(`${photo} deleted`);
      }
    );
  }
};

exports.getAllOwners = async (req, res, next) => {
  const queryObj = { ...req.query };
  const excludeFields = ["sort", "page", "limit"];
  excludeFields.forEach((el) => delete queryObj[el]);

  const total = await Owner.countDocuments();
  const page = +req.query.page || 1;
  const limit = +req.query.limit || 10;
  const startIndex = (page - 1) * limit; // 2 - 1 * 1 = 1 // skip
  const endIndex = page * limit; // 2 * 1 = 2 //

  const pagination = {};
  if (endIndex < total) {
    // 2 < 4 -> true
    pagination.next = {
      page: page + 1, // 3
    };
  }
  if (startIndex > 0) {
    // 1 > 0 -> true
    pagination.previous = {
      page: page - 1, // 1
    };
  }

  Owner.find(
    startIndex,
    limit,
    (sort = req.query.sort),
    (search = queryObj),
    (err, result) => {
      if (err) {
        return res.status(500).json({
          success: false,
          message: "Internal server error",
        });
      }

      res.status(200).json({
        success: true,
        totalRecords: total,
        per_page_limit: limit,
        pagination,
        data: {
          owners: result,
        },
      });
    }
  );
};

exports.getOwner = (req, res, next) => {
  const id = +req.params.id;

  if (isNaN(id)) {
    return res.status(400).json({
      success: false,
      message: `Invalid id ${req.params.id}`,
    });
  }

  Owner.findById(id, (err, result) => {
    if (err) {
      return res.status(500).json({
        success: false,
        message: "Internal server error",
      });
    }
    if (result.length === 0) {
      return res.status(404).json({
        success: true,
        totalRecords: result.length,
        message: `No record found with id ${id}`,
      });
    }
    res.status(200).json({
      success: true,
      data: {
        owner: result[0],
      },
    });
  });
};

exports.createOwner = (req, res, next) => {
  // check null
  if (
    !req.body.name ||
    !req.body.email ||
    !req.body.mobile_number ||
    !req.body.hobbies ||
    !req.body.gender
  ) {
    return res.status(400).json({
      success: false,

      message:
        "name, email, mobile_number, hobbies, gender must be required fields",
    });
  }
  if (!validator.isEmail(req.body.email)) {
    return res.status(400).json({
      success: false,

      message: `Invalid email ${req.body.email}`,
    });
  }
  if (req.body.mobile_number.toString().length !== 10) {
    return res.status(400).json({
      success: false,

      message: `Invalid mobile number ${req.body.mobile_number}`,
    });
  }
  if (req.files) {
    const response = handelFile(req, res);
    if (response) {
      return response;
    }
  }

  const owner = new Owner(req.body);

  Owner.create(owner, (err, newOwner) => {
    if (err) {
      return res.status(err.statusCode).json({
        success: false,

        message: err.message,
      });
    }
    if (req.files) {
      const response = movePhotoToStorage(req.files.photo);
      if (response) {
        return response;
      }
    }
    res.status(201).json({
      success: true,
      data: {
        ownerId: newOwner.insertId,
      },
    });
  });
};

exports.updateOwner = (req, res, next) => {
  const id = +req.params.id;
  if (
    !req.body.name ||
    !req.body.email ||
    !req.body.mobile_number ||
    !req.body.hobbies ||
    !req.body.gender
  ) {
    return res.status(400).json({
      success: false,
      message:
        "name, email, mobile_number, hobbies, gender must be required fields",
    });
  }
  if (isNaN(id)) {
    return res.status(400).json({
      success: false,

      message: `Invalid id ${req.params.id}`,
    });
  }
  if (req.body.email) {
    if (!validator.isEmail(req.body.email)) {
      return res.status(400).json({
        success: false,

        message: `Invalid email ${req.body.email}`,
      });
    }
  }
  if (req.body.mobile_number) {
    if (req.body.mobile_number.toString().length !== 10) {
      return res.status(400).json({
        success: false,

        message: `Invalid mobile number ${req.body.mobile_number}`,
      });
    }
  }

  Owner.findById(id, (err, result) => {
    if (err) {
      return res.status(500).json({
        success: false,
        message: "Internal server error",
      });
    }
    if (result.length === 0) {
      return res.status(404).json({
        success: true,
        totalRecords: result.length,
        message: `No record found with id ${id}`,
      });
    }
    const owner = new Owner(req.body);
    Owner.findByIdAndUpdate(id, owner, (err, result) => {
      if (err) {
        return res.status(err.statusCode).json({
          success: false,
          message: err.message,
        });
      }
      res.status(200).json({
        success: true,

        message: `Record with id ${id} updated successfully`,
      });
    });
  });
};

exports.deleteOwner = (req, res, next) => {
  let ids = req.params.id.split(",").map((el) => +el);

  ids.forEach((id) => {
    Owner.findById(id, (err, result) => {
      if (!err) {
        Owner.findByIdAndDelete(id, (err, deletedRecord) => {
          if (!err) {
            deletePhotoFromStorage(result[0].photo);
          }
        });
      }
    });
  });
  return res.status(200).json({
    success: true,
    message: `Record with id ${req.params.id} deleted successfully`,
  });
};

exports.updateImage = (req, res, next) => {
  if (!req.files) {
    return res.status(400).json({
      success: false,

      message: "Image must be required",
    });
  }

  const id = +req.params.id;

  if (isNaN(id)) {
    return res.status(400).json({
      success: false,

      message: `Invalid id ${req.params.id}`,
    });
  }

  Owner.findById(id, (err, result) => {
    if (err) {
      return res.status(500).json({
        success: false,

        message: "Internal server error",
      });
    }
    if (result.length === 0) {
      return res.status(404).json({
        success: true,
        totalRecords: result.length,
        message: `No record found with id ${id}`,
      });
    }

    let response = handelFile(req, res);
    if (response) {
      return response;
    }
    Owner.findByIdAndUpdateImage(id, req.body.photo, (err, upadtedInfo) => {
      if (err) {
        return res.status(500).json({
          success: false,

          message: "internal server error",
        });
      } else {
        response = movePhotoToStorage(req.files.photo);
        if (response) {
          return response;
        }
        deletePhotoFromStorage(result[0].photo);

        res.status(200).json({
          success: true,

          message: `photo updated successfully of owner id ${id}`,
        });
      }
    });
  });
};
