const dbCon = require("../config/db.config");

const Hobbies = function () {};

Hobbies.create = (hobbies, owner_id, result) => {
  const values = [];
  hobbies.forEach((hobby) => {
    values.push([hobby, owner_id]);
  });
  dbCon.query(
    `INSERT INTO hobbies (hobbies_id, owner_id) VALUES ? `,
    [values],
    (err, res) => {
      if (err) {
        result(
          {
            message: "something went wrong",
            statusCode: 500,
          },
          null
        );
      } else {
        result(null, res);
      }
    }
  );
};

Hobbies.update = function (hobbies, owner_id, result) {
  dbCon.query(
    `DELETE FROM hobbies WHERE owner_id = ? `,
    [owner_id],
    (err, res) => {
      if (err) {
        result(
          {
            message: "something went wrong",
            statusCode: 500,
          },
          null
        );
      } else {
        this.create(hobbies, owner_id, (err, res) => {
          if (err) {
            result(
              {
                message: "something went wrong",
                statusCode: 500,
              },
              null
            );
          } else {
            result(null, res);
          }
        });
      }
    }
  );
};

module.exports = Hobbies;
