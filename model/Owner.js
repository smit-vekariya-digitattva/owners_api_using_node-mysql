const dbCon = require("../config/db.config");
const Hobbies = require("../model/Hobbies");

const Owner = function (owner) {
  this.name = owner.name;
  this.email = owner.email;
  this.mobile_number = owner.mobile_number;
  this.bio = owner.bio;
  this.hobbies =
    typeof owner.hobbies === "string"
      ? owner.hobbies.split(" ")
      : owner.hobbies;
  this.active_status = owner.active_status || 0;
  this.role = owner.role || "student";
  this.gender = owner.gender;
  this.photo = owner.photo || "user.png";
  this.created_at = owner.created_at || new Date();
};

const errorGenerator = (err, owner) => {
  let message = "Internal server error",
    statusCode = 500;
  if (err.code === "ER_DATA_TOO_LONG") {
    statusCode = 400;
    if (err.sqlMessage.includes("'mobile_number'")) {
      message = `invalid mobile number`;
    }
    if (err.sqlMessage.includes("'name'")) {
      message = `name field allows 50 characters only`;
    }
    if (err.sqlMessage.includes("'email'")) {
      message = `email field allows 50 characters only`;
    }
  }
  if (err.code === "ER_DUP_ENTRY") {
    statusCode = 400;
    if (err.sqlMessage.endsWith("'owners.mobile_number'")) {
      message = `Mobile_number ${owner.mobile_number} is already registerd`;
    }
    if (err.sqlMessage.endsWith("'owners.email'")) {
      message = `email ${owner.email} is already registerd`;
    }
  }
  if (err.code === "WARN_DATA_TRUNCATED") {
    statusCode = 400;
    if (err.sqlMessage.includes("'gender'")) {
      message = `gender must be either male, female, other. but you have typed ${owner.gender}`;
    }
    if (err.sqlMessage.includes("'role'")) {
      message = `role must be either student, teacher, contractor. but you have typed ${owner.role}`;
    }
  }
  return {
    message,
    statusCode,
  };
};

Owner.find = (startIndex, limit, sort, search, result) => {
  let where = "";
  Object.entries(search).forEach(([key, value], index) => {
    if (index === 0) {
      where += `where o.${key} regexp '${value}' `;
    } else {
      where += `and o.${key} regexp '${value}' `;
    }
  });
  dbCon.query(
    `select o.*, group_concat(hn.name) as hobbies from owners o join hobbies h using(owner_id)
     join hobbies_names hn using(hobbies_id) ${where} group by o.owner_id order by ${sort} LIMIT ${startIndex}, ${limit} `,

    (err, res) => {
      if (err) {
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

Owner.findById = (id, result) => {
  dbCon.query(
    `select o.*, group_concat(hn.name) as hobbies from owners o join hobbies h using(owner_id) join hobbies_names hn using(hobbies_id) where o.owner_id = ?  group by o.owner_id  `,
    id,
    (err, res) => {
      if (err) {
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

Owner.create = (owner, result) => {
  const ownerHobbies = owner.hobbies;
  delete owner.hobbies;
  dbCon.query(`INSERT INTO owners set ? `, owner, (err, res) => {
    if (err) {
      if (err) {
        const { message, statusCode } = errorGenerator(err, owner);
        result({ message, statusCode }, null);
      }
    } else {
      Hobbies.create(ownerHobbies, res.insertId, (err, innerRes) => {
        if (err) {
          result(
            {
              message,
              statusCode,
            },
            null
          );
        } else {
          result(null, res);
        }
      });
    }
  });
};

Owner.findByIdAndUpdate = (id, owner, result) => {
  dbCon.query(
    "UPDATE owners SET name = ?, email = ?, mobile_number = ?, bio = ?, active_status = ?, role = ?, gender = ? WHERE owner_id = ? ",
    [
      owner.name,
      owner.email,
      owner.mobile_number,
      owner.bio,
      owner.active_status,
      owner.role,
      owner.gender,
      id,
    ],
    (err, res) => {
      if (err) {
        const { message, statusCode } = errorGenerator(err, owner);
        result({ message, statusCode }, null);
      } else {
        if (typeof owner.hobbies === "object") {
          Hobbies.update(owner.hobbies, id, (err, res) => {
            if (err) {
              result(
                { message: err.message, statusCode: err.statusCode },
                null
              );
            } else {
              result(null, res);
            }
          });
        } else {
          result(null, res);
        }
      }
    }
  );
};

Owner.findByIdAndDelete = (id, result) => {
  dbCon.query(`DELETE FROM owners WHERE owner_id = ?`, id, (err, res) => {
    if (err) {
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Owner.countDocuments = () => {
  return new Promise((resolve, reject) => {
    dbCon.query(
      `SELECT COUNT(*) as total_records from owners`,
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result[0].total_records);
        }
      }
    );
  });
};

Owner.findByIdAndUpdateImage = (id, photo, result) => {
  dbCon.query(
    `UPDATE owners SET photo = ? where owner_id = ?`,
    [photo, id],
    (err, res) => {
      if (err) {
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

module.exports = Owner;
