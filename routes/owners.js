const express = require("express");
const router = express.Router();

const {
  getAllOwners,
  getOwner,
  createOwner,
  updateOwner,
  deleteOwner,
  updateImage,
} = require("../controllers/ownersController");

/**
 * @swagger
 * definitions:
 *  Owner:
 *    type: object
 *    required:
 *      - name
 *      - email
 *      - mobile_number
 *      - hobbies
 *      - gender
 *    properties:
 *      name:
 *        type: string
 *        description: Owner name
 *        example: 'smit vekariya'
 *      email:
 *        type: string
 *        format: email
 *        description: Email of the Owner
 *        example: 'smit.vekariya@xyz.com'
 *      mobile_number:
 *        type: string,
 *        description: mobile_number of the Owner
 *        example: '1234567899'
 *      gender:
 *        type: string
 *        description: Gender of the owner
 *        example: 'male'
 *        enum: [male, female, other]
 *      bio:
 *        type: string
 *        description: Biography of the owner
 *        example: 'hello, i am a full stack web developer'
 *      hobbies:
 *        type: array
 *        items:
 *          type: int
 *        description: 1- reading, 2- writting, 3- designing, 4- swimming, 5- developing 6- skydiving
 *        example: [1, 2, 5]
 *      role:
 *        type: string
 *        description: role of the owner
 *        enum: [student, teacher, contractor]
 *        example: 'student'
 */

router
  .route("/")

  /**
   * @swagger
   * /api/v1/owners:
   *   get:
   *     tags:
   *        - owners
   *     description: Returns all owners
   *     produces:
   *        - application/json
   *     parameters:
   *        - in: query
   *          name: page
   *          description: page number
   *        - in: query
   *          name: limit
   *          description: limits data per page
   *     responses:
   *       200:
   *         description: An array of owners
   *       404:
   *         description: No owners found
   *
   */
  .get(getAllOwners)
  /**
   * @swagger
   * /api/v1/owners:
   *   post:
   *    tags:
   *      - owners
   *    description: Create new owner
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: owner
   *        description: owner object
   *        in: body
   *        required: true
   *        type: object
   *        schema:
   *          $ref: '#definitions/Owner'
   *    responses:
   *      201:
   *        description: Successfully created with owner info
   *      400:
   *        description: If invalid data provided
   *
   */
  .post(createOwner);
router
  .route("/:id")
  /**
   * @swagger
   * /api/v1/owners/{id}:
   *  get:
   *    tags:
   *      - owners
   *    decription: Reaturn single owner
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: id
   *        description: Owner Id
   *        in: path
   *        required: true
   *        type: string
   *    responses:
   *      200:
   *        description: A single owner
   *      404:
   *        description: No owner found
   *      400:
   *        description: If passing Invalid id
   */
  .get(getOwner)
  /**
   * @swagger
   * /api/v1/owners/{id}:
   *  patch:
   *    tags:
   *      - owners
   *    description: Update owner info
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: id
   *        description: Owner Id
   *        in: path
   *        required: true
   *        type: string
   *      - name: owner
   *        description: owner object
   *        in: body
   *        required: true
   *        type: object
   *        schema:
   *          $ref: '#definitions/Owner'
   *    responses:
   *      200:
   *        description: Owners updated info
   *      404:
   *        description: No owner found
   *      400:
   *        description: If passing Invalid id
   *
   */
  .patch(updateOwner)
  /**
   * @swagger
   * /api/v1/owners/{id}:
   *  delete:
   *    tags:
   *      - owners
   *    description: delete owner
   *    parameters:
   *      - name: id
   *        description: owner Id
   *        in: path
   *        required: true
   *        type: string
   *    responses:
   *      204:
   *        description: Owner deleted
   *      404:
   *        description: No owner found
   *      400:
   *        description: If passing Invalid id
   */
  .delete(deleteOwner);

/**
 * @swagger
 * /api/v1/owners/{id}/update/image:
 *  patch:
 *    tags:
 *      - owners
 *    description: update owner image
 *    parameters:
 *      - in: formData
 *        name: photo
 *        type: file
 *        required: true,
 *        description: new photo
 *      - name: id
 *        description: owner Id
 *        in: path
 *        required: true
 *        type: string
 *    responses:
 *      200:
 *        description: photo updated
 *      400:
 *        description: invalid data
 *      404:
 *        description: owner not found
 */
router.route("/:id/update/image").patch(updateImage);

module.exports = router;
