require("dotenv").config();

const path = require("path");

const express = require("express");
const cors = require("cors");
const app = express();
const PORT = process.env.PORT || 3001;
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const morgan = require("morgan");
const fileUpload = require("express-fileupload");
const ownersRoute = require("./routes/owners");

// application/json
app.use(express.json());

// application/x-www-form-rulencoded
app.use(express.urlencoded({ extended: false }));

// cors
app.use(cors());

// File handel
app.use(fileUpload());

// Swagger
const options = {
  swaggerDefinition: {
    info: {
      title: "Owners API",
      version: "1.0.0",
      description: "This api is used to make CRUD operations on owners...",
    },
    host: "127.0.0.1:3001",
    basepath: "/",
  },
  apis: ["./routes/owners.js"],
};

const swaggerSpecification = swaggerJsdoc(options);
app.use(morgan("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use((req, res, next) => {
  res.header("x-powered-by", "Smitt Vekariya");
  next();
});
app.use("/api/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpecification));
app.get("/", (req, res, next) => {
  res.redirect("/api/docs");
});
app.use("/api/v1/owners", ownersRoute);

app.listen(PORT, console.log("Server running on port", PORT));
